project(testing)

add_executable(unit_test_example
  unit_test_example.cpp
  test_with_deps_example.cpp
  defeating_time_example.cpp
  avoiding_threads.cpp
  busy_waiting.cpp)

target_link_libraries(unit_test_example
  gtest gmock gmock_main pthread dl)
