#include <queue>
#include <mutex>
#include <condition_variable>

class FixedQueue {
public:
    FixedQueue(size_t max_size) : max_size_(max_size) {}

    void Push(int elem) {
        std::unique_lock guard(mutex_);
        not_full_.wait(guard, [this] {
            return queue_.size() < max_size_;
        });
        queue_.push(elem);
        not_empty_.notify_one();
    }

    int Pop() {
        std::unique_lock guard(mutex_);
        not_empty_.wait(guard, [this] {
            return queue_.size() > 0;
        });
        auto res = queue_.front();
        queue_.pop();
        not_full_.notify_one();
        return res;
    }


private:
    std::queue<int> queue_;
    std::mutex mutex_;
    size_t max_size_;
    std::condition_variable not_full_, not_empty_;
};
